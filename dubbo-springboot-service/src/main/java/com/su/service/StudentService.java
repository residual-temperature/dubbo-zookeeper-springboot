package com.su.service;
import com.su.pojo.Student;

public interface StudentService {

    //根据学生id查询学生信息
    public Student findStudentById(int id);

}
