package com.su.consumer.controller;
import com.su.pojo.Student;
import com.su.service.StudentService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class StudentController {

    //与之前提供者提供的版本号对应
    @DubboReference(version = "1.0.0")
    private StudentService studentService;

    @GetMapping("/get")
    public Student findStudentById(int id){
       return studentService.findStudentById(id);
    }

}
